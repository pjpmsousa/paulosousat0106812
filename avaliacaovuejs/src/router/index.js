import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import MoviesIndex from '@/components/MoviesIndex'
import MoviesCreate from '@/components/MoviesCreate'
import MoviesCheckYear from '@/components/MoviesCheckYear'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/movies/index',
      name:'movies.index',
      component: MoviesIndex
    },
    {
      path: '/movies/create',
      name: 'movies.create',
      component: MoviesCreate
    },
    {
      path: '/movies/checkyear',
      name: 'movies.checkyear',
      component: MoviesCheckYear
    }
  ]
})
